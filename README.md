# README #

L'application charte Informatique permet de faire valider une charte à un ensemble d'utilisateurs de façon dématérialisé. les utilisateurs reçoivent un courriel les renvoyant sur un site web pour valider la charte. Via un tableau de bord l'administrateur peut consulter les utilisateurs ayant ou pas validés la charte numerique.

## Fonctionnalités ##
* Envoi d'email automatique pour les utilisateurs 
* Tableau de bord permettant d'administrer les utilisateurs 
* creation, suppression , modification d' utilisateurs 


## Téléchargement ##

Pour télécharger l'application , cliquez sur le lien suivant:

https://bitbucket.org/lyceenature/charte_informatique/downloads

## Installation ##

Un serveur web avec PHP et Mysql est necessaire.

 * décompresser l'application sur un serveur web, dans /var/www/charte 
 * Créer une base de donnée sous Mysql appelé charte puis importer e fichier sql charte.sql 
 * Configurer les acces à la base de données dans le fichier Constantes.php 
 * Configurer les acces d'envoi de courriel dans le fichier Constantes.php 
 * Connectez vous avec le login admin et le mot de passe admin sur http://serveur/charte 
 * Importer les utilisateurs au format csv comme indiqué dans le fichier pdf :DCTgestion-de-la-charte-informatique.pdf 

pascal Lamy