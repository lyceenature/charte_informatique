 <!DOCTYPE html>
 <html>
<head>
	<meta charset="UTF-8">
	<title>Validation de la Charte Informatique</title>

<link rel="stylesheet" type="text/css"href="js/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="js/themes/icon.css">
<link rel="stylesheet" type="text/css" href="css/stylev.css">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="js/easyloader.js"></script>
<script type="text/javascript">
$('document').ready(function() {
   easyloader.locale = 'fr';
});
</script>

	</head>
	<body>
		
	<img src="images/logo.png" width="279" height="165" border="0" alt="" title="" />
<h2>Validation de la Charte Informatique</h2>
<?php
include "utilAuth.php";
$email= isset($_GET['email']) ? strval($_GET['email']) : null;
$token= isset($_GET['token']) ? strval($_GET['token']) : null;
			
			if($email != null && $token!=null){
				$mes="Veuillez valider la charte informatique en cochant la case à cocher puis en cliquant le sur le bouton valider. ";
				utilAuth::mesInfo($mes);
				
			}else

	{ $mes="Erreur d'Authentification ou votre session a expiree! Veuillez vous reconnecter!";
		utilAuth::mesInfo($mes);
		echo "</body></html>";
		exit;
	}


?>
<fieldset class="terms-of-service form-wrapper" #weight="0" id="edit-extra-pane-node-104"><legend><span class="fieldset-legend">Charte Informatique</span></legend>
 

<p align="center" style="margin-top: 0.42cm; margin-bottom: 0.21cm; line-height: 115%; page-break-after: avoid">
<font size="4" style="font-size: 16pt"><b>Charte
d'utilisation du mat&eacute;riel informatique et d'Internet </b></font>
</p>
<p   align="justify" style="margin-top: 0cm; line-height: 115%">
<br/>

</p>
<p  >La pr&eacute;sente charte a pour objet de d&eacute;finir
les r&egrave;gles de fonctionnement de l'ensemble du mat&eacute;riel
informatique de l' EPLEFPA Nature &ndash; La Roche Sur Yon. Elle
s'inscrit dans le cadre des lois en vigueur&nbsp;:</p>
<p   style="margin-top: 0cm; line-height: 115%"><br/>

</p>
<ul>
	<li/>
<p  ><i>Loi no. 78-17 du 6 janvier 1978
	&laquo;&nbsp;informatique, fichiers et libert&eacute;s&nbsp;&raquo;</i></p>
<li/>
<p  ><i>	Loi no. 78-753 du 17 juillet 1978 sur l'acc&egrave;s
aux documents administratifs</i></p>
<li/>
<p  ><i>	Loi no. 85-660 du 3 juillet 1985 sur la
protection des logiciels</i></p>
<li/>
<p  ><i>	Loi no. 88-19 du 5 janvier 1988 relative &agrave;
la fraude informatiques</i></p>
<li/>
<p  ><i>	Loi no. 92-597 du 1er juillet 1992  (code pour
la propri&eacute;t&eacute; intellectuelle)</i></p>
<li/>
<p  ><i>	Loi no. 2004-575 du 21 juin 2004 pour la
confiance dans l'&eacute;conomie num&eacute;rique</i></p>

	<li/>
<p  ><i>Note de Service DGA/SDSI/MSSI/N200561076
	CAB/MD/N2005-0002 du  18/02/2005 sur la s&eacute;curit&eacute; des
	syst&egrave;mes d'information &ndash; droits et devoirs des
	utilisateurs du r&eacute;seau du MAAPR</i></p>
</ul>
<p   align="justify" style="margin-top: 0cm; line-height: 115%">
<br/>

</p>
<p   align="justify" style="margin-top: 0cm; line-height: 115%">
<br/>

</p>
<h1  >1. DOMAINES D'APPLICATION :</h1>
<p  >Ce r&egrave;glement s'applique &agrave; toute
personne utilisant les syst&egrave;mes informatiques de
l'&eacute;tablissement (&eacute;l&egrave;ves, &eacute;tudiants,
enseignants, stagiaires, apprentis, personnels administratifs ou
techniques)</p>
<p   align="justify" style="margin-top: 0cm; line-height: 115%">
<br/>

</p>
<h1  >2. Conditions d'acc&egrave;s aux moyens
informatiques</h1>
<p  >L'utilisation des moyens informatiques de
l'&eacute;tablissement doit &ecirc;tre limit&eacute; &agrave; des
activit&eacute;s dont l<span style="font-weight: normal">'int&eacute;r&ecirc;t
p&eacute;dagogique</span> (valid&eacute; par l'&eacute;quipe
&eacute;ducative) ou professionnel.</p>
<p  >Un compte informatique <span style="font-weight: normal">individuel
</span>est attribu&eacute; &agrave; chaque utilisateur. Celui-ci
pourra se connecter au r&eacute;seau informatique par le biais d'un
serveur d'authentification qui lui d&eacute;livrera ainsi l'acc&egrave;s
&agrave; son r&eacute;pertoire personnel.</p>
<p  >Les comptes et mots de passe sont nominatifs et
personnels, incessibles et disparaissent lorsque son titulaire quitte
l' EPLEFPA. Chaque utilisateur est responsable de l'utilisation de
son compte.</p>
<p  >L'utilisateur pr&eacute;viendra le technicien
informatique si son mot de passe ne lui permet plus de se connecter
ou s'il soup&ccedil;onne que son compte est utilis&eacute; par une
autre personne.</p>
<p  ><br/>

</p>
<p   align="justify" style="margin-top: 0cm; line-height: 115%">
<br/>

</p>
<h1  >3. Le respect des r&egrave;gles</h1>
<h2  >3.1R&egrave;gles de bases</h2>
<p  >Chaque utilisateur s'engage &agrave; respecter les
r&egrave;gles de la d&eacute;ontologie informatique et notamment &agrave;
ne pas effectuer intentionnellement des op&eacute;rations qui
pourraient avoir pour cons&eacute;quences&nbsp;:</p>
<p   align="justify" style="margin-top: 0cm; line-height: 115%">
<br/>

</p>
<ul>
	<li/>
<p  >de s'approprier le mot de passe d'un autre
	utilisateur&nbsp;;</p>
	<li/>
<p  >de modifier ou de d&eacute;truire des
	informations ne lui appartenant pas sur l'un des syst&egrave;mes
	informatiques&nbsp;;</p>
	<li/>
<p  >d'acc&eacute;der &agrave; des informations
	appartenant &agrave; d'autres utilisateurs sans leur autorisation&nbsp;;</p>
	<li/>
<p  >de porter atteinte &agrave; l'int&eacute;grit&eacute;
	d'un autre utilisateur ou &agrave; sa sensibilit&eacute;&nbsp;;</p>
	<li/>
<p  >d'interrompre le fonctionnement normal du
	r&eacute;seau ou d'un des syst&egrave;mes connect&eacute;s ou non au
	r&eacute;seau.</p>
</ul>
<p  ><br/>

</p>
<h2  >3.2Utilisation d'Internet</h2>
<p  >L'acc&egrave;s &agrave; Internet est fourni par
l'&eacute;tablissement dans un cadre p&eacute;dagogique ou
professionnel.</p>
<p  >Les activit&eacute;s sur Internet doivent &ecirc;tre
conformes &agrave; la l&eacute;gislation fran&ccedil;aise en vigueur
et &agrave; la d&eacute;ontologie d'un Etablissement Public
d'Enseignement et de Formation. La loi n'autorise pas la publication
ou la diffusion de messages &agrave; caract&egrave;res injurieux,
pornographiques, diffamatoires, d'incitation au racisme ou &agrave;
des fins de propagande.</p>
<p  >L'&eacute;tablissement filtre l'acc&egrave;s aux
sites pornographiques et se r&eacute;serve la possibilit&eacute; de
consulter l'historique des diff&eacute;rents sites et services
Internet consult&eacute;s par les utilisateurs afin de v&eacute;rifier
leur ad&eacute;quation au pr&eacute;sent r&egrave;glement.</p>
<p  >Les acc&egrave;s WIFI sont d&eacute;sactiv&eacute;s
la nuit.</p>
<p  ><br/>

</p>
<h2  >3.3Utilisation de logiciels et respect des droits
de la propri&eacute;t&eacute;</h2>
<p  > Il est strictement interdit aux utilisateurs de
r&eacute;aliser des copies de tout logiciel autre que ceux du domaine
public et d'en faire un usage non conforme aux prescriptions de son
auteur ou de la soci&eacute;t&eacute; qui le met &agrave;
disposition.</p>
<p  >L'installation de logiciels sur les ordinateurs de
l'EPLEFPA est r&eacute;serv&eacute; aux personnes autoris&eacute;es,
membres de l'&eacute;quipe &eacute;ducative.</p>
<p  >Il est interdit aux utilisateurs d'installer des
logiciels sur les ordinateurs de l'EPLEFPA, sans autorisation
pr&eacute;alable. L'utilisateur ne devra en aucun cas essayer toute
man&oelig;uvre permettant de contourner les restrictions
d'utilisation d'un logiciel.</p>
<p  >La source des documents r&eacute;cup&eacute;r&eacute;s
et r&eacute;-int&eacute;gr&eacute;s dans divers documents &eacute;labor&eacute;s
par les usages doit &ecirc;tre cit&eacute;e.</p>
<p  ><br/>

</p>
<h2  >3.4Bonne utilisation des moyens informatiques</h2>
<p  >Chaque utilisateur s'engage &agrave; prendre soin
du mat&eacute;riel et des locaux informatiques mis &agrave; sa
disposition. Il informera son enseignant ou le technicien
informatique de toute anomalie constat&eacute;e.</p>
<p  >L'utilisateur s'engage &agrave; ne pas apporter
volontairement des perturbations au syst&egrave;me informatique, soit
par des manipulations anormales du mat&eacute;riel, soit par
l'introduction de logiciels parasites tels que des virus, vers,
chevaux de Troie, bombes logiques, spywares, malwares ou autre. La
s&eacute;curit&eacute; est l'affaire de tous, chaque utilisateur de
l'informatique et du r&eacute;seau de l'&eacute;tablissement doit y
contribuer et mettre en application les r&egrave;gles de bon sens et
les recommandations fournies par le responsable de l'outil
informatique. 
</p>
<p  >Chaque utilisateur s'engage &agrave; prendre
connaissance et &agrave; respecter toutes les r&egrave;gles
suppl&eacute;mentaires sp&eacute;cifiques &agrave; des lieux pour
l'utilisation des moyens informatiques (CDI, CDR ...)</p>
<p  >Quand un utilisateur s'est connect&eacute; &agrave;
un poste de travail, il doit se d&eacute;connecter (en fermant sa
session de travail en le quittant). La proc&eacute;dure &agrave;
suivre lui sera indiqu&eacute;e.</p>
<p  ><br/>

</p>
<p   align="justify" style="margin-top: 0cm; line-height: 115%">
<br/>

</p>
<h1  >4.  RESPONSABILITE DE L'ETABLISSEMENT :</h1>
<p  >L'&eacute;tablissement ne pourra &ecirc;tre tenu
responsable de toute d&eacute;t&eacute;rioration d'informations du
fait d'un utilisateur qui ne serait pas conform&eacute; &agrave;
l'engagement qu'il a sign&eacute; ou du fait d'une panne mat&eacute;rielle
ou logicielle. La confidentialit&eacute; totale des informations de
l'utilisateur ne peut &ecirc;tre garantie : des individus mal
intentionn&eacute;s (pirates) peuvent acc&eacute;der &agrave;  ces
informations. L'&eacute;tablissement ne fournit aucune garantie,
implicite ou explicite, quant &agrave; l'exactitude des r&eacute;sultats
obtenus par l'utilisation de ses moyens informatiques.</p>
<p   align="justify" style="margin-top: 0cm; line-height: 115%">
<br/>

</p>
<p   align="justify" style="margin-top: 0cm; line-height: 115%">
<br/>

</p>
<p   align="justify" style="margin-top: 0cm; line-height: 115%">
<br/>

</p>
<p   align="justify" style="margin-top: 0cm; line-height: 115%">
<br/>

</p>
<p   align="justify" style="margin-top: 0cm; line-height: 115%">
<br/>

</p>
<h1  >5. ENCADREMENT :</h1>
<p  >Les ordinateurs des salles informatiques ne sont
utilisables qu'en pr&eacute;sence d'une personne responsable.</p>
<p  >La vie scolaire g&egrave;re l'acc&egrave;s de la
salle libre service.</p>
<p  >La Direction de l'&eacute;tablissement ainsi que
le responsable informatique se r&eacute;servent le droit : de prendre
le contr&ocirc;le &agrave; distance des ordinateurs, de lire les
journaux d'activit&eacute; du service d'acc&egrave;s r&eacute;seau,
afin de v&eacute;rifier que l'utilisation qui en est faite est
conforme au pr&eacute;sent r&egrave;glement.</p>
<p  ><br/>

</p>
<p   align="justify" style="margin-top: 0cm; line-height: 115%">
<br/>

</p>
<h1  >6. BOISSONS ET AUTRES :</h1>
<p  >L'utilisateur doit &eacute;galement s'abstenir
d'introduire, dans les salles disposant d'ordinateurs, des boissons
sous n'importe quelle forme que ce soit (bouteille, gobelet, canette,
.....) ou tout autre r&eacute;cipient susceptible de contenir des
produits pouvant endommager le mat&eacute;riel informatique.</p>
<p   align="justify" style="margin-top: 0cm; line-height: 115%">
<br/>

</p>
<h1  >7. SANCTIONS APPLICABLES :</h1>
<p   align="left">Le responsable informatique n'ouvre
de compte qu'aux utilisateurs ayant pris connaissance et sign&eacute;
le pr&eacute;sent document, et peut le fermer s'il a des raisons de
penser que l'utilisateur viole les r&egrave;gles &eacute;nonc&eacute;es
ici. S'agissant des &eacute;l&egrave;ves mineurs, l'adh&eacute;sion &agrave;
la charte et l'approbation de ses r&egrave;gles ne peuvent &ecirc;tre
acquises que par l'effet de la signature de cette charte par la ou
les personnes majeures b&eacute;n&eacute;ficiant sur lui de
l'autorit&eacute; l&eacute;gale pour le repr&eacute;senter.
L'utilisateur qui ne respecterait pas ces r&egrave;gles, pourra se
voir exclu de la salle informatique et s'expose &agrave;
l'interdiction de l'acc&egrave;s &agrave; Internet ou au retrait de
son compte informatique. Il est, en outre, passible de poursuites,
internes &agrave; l'EPL (disciplinaires) ou p&eacute;nales (lois du 6
janvier 1978, du 3 juillet 1985 et du 5 janvier 1988).</p>
<p  >L'&eacute;tablissement se r&eacute;serve le droit
de fermer ponctuellement la salle d'acc&egrave;s libre service si on
constate des d&eacute;gradations et/ou vols de mat&eacute;riels.</p>
<p  ><br/>

</p>
<p  ><br/>

</p>
<p  ><br/>

</p>


<div >
<form method="POST" action="validerCharte.php">	
	        
 <input type="checkbox" class="easyui-validatebox" name="accepter" value="1" required="true" />  <label >J'accepte la Charte</label>
<input type="hidden" name="email" value="<?php echo $email; ?>"  />
<input type="hidden" name="token" value="<?php echo $token; ?>"  />
</div>
</div></fieldset>
<fieldset ><input  type="submit" class="easyui-linkbutton" icon="icon-ok" value="VALIDER" /></div></fieldset>


</div></form>  </div>
</body>
</html>
