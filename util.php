<?php
include_once "Constantes.php";
include_once "pdo/personneDB.php";
include_once "pdo/connectionPDO.php";
class util{
	//message info
	public static function mesInfo($mes){
		echo '<div class="mes-info">';
		echo '	<div class="mes-tip icon-tip"></div>';
		echo '<div>'.$mes.'</div></div>';

	}
	
	public static function couriel($to,$from, $sujet,$message){
	
     $headers = 'From: '.$from . "\r\n" .
     'Reply-To: '.$from . "\r\n" .
     'X-Mailer: PHP/' . phpversion();

     mail($to, $sujet, $message, $headers);
		
	}
public static function courielEducagri($to,$token){
		
require_once 'PHPMailer/PHPMailerAutoload.php';


//Create a new PHPMailer instance
$mail = new PHPMailer;
//Tell PHPMailer to use SMTP
$mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 0;
//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';
//Set the hostname of the mail server
$mail->Host =  Constantes::HOST_MAIL;
//Set the SMTP port number - likely to be 25, 465 or 587
$mail->Port = 587;
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
$mail->SMTPSecure = 'tls';
//Username to use for SMTP authentication
$mail->Username = Constantes::AUTH_USERMAIL;
//Password to use for SMTP authentication
$mail->Password = Constantes::AUTH_PASSMAIL;
//Set who the message is to be sent from
$mail->setFrom(Constantes::FROM, Constantes::FROMALIAS);
//Set an alternative reply-to address
$mail->addReplyTo(Constantes::NOREPLY, Constantes::NOREPLYALIAS);
//Set who the message is to be sent to
$mail->addAddress($to, $to);
//Set the subject line
$mail->Subject = Constantes::SUJET;
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
//$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
$body=Constantes::MESSAGE1 ."<a href=\"".Constantes::SITECHARTE.$to."&token=".$token."\">".Constantes::SITECHARTE.$to."&token=".$token."</a>";
$body .=Constantes::MESSAGE2;
$body .=Constantes::MESSAGE3;
$body .=Constantes::MESSAGE4;
$mail->msgHTML($body);
//Replace the plain text body with one created manually
//$mail->AltBody = 'This is a plain-text message body';
//Attach an image file
//$mail->addAttachment('images/phpmailer_mini.png');

//send the message, check for errors
if (!$mail->send()) {
    echo "<br/>Mail Erreur: ".$to . $mail->ErrorInfo;
} else {
    echo "<br/>Message envoy� � $to";
}

	}
	
	
	
	

	public static function auth(){
		if(isset($_SESSION['token']) && isset($_SESSION['token_time']) && isset($_GET['id']))
		{
			 
			if($_SESSION['token'] == $_GET['id'])
			{
				//On stocke le timestamp il y a 15 minutes
				$timestamp_ancien = time() - (30*60);
				//Si le jeton n'est pas expir�
				if($_SESSION['token_time'] >= $timestamp_ancien){
					return true;
				}
				return false;
			}
			return false;
		}
		return false;
	}
	//liste des clients
	public static function listeClient($pda){
		
		$persBDD=new personneDB($pda);
	$res=$persBDD->selectListePersonne(1,'nom','ASC',0,1000);
	$liste=""; 
	 foreach ($res as $key2 => $value2){
	 	
	 	 	$liste.="<option value=\"".$key2."\">";
	foreach ($value2 as $key => $value){
	
			if($key=="nom"){
				$liste.=$value["nom"];
			}
			if( $key=="prenom"){
				$liste.=" ".$value["prenom"];
			}
		}
		$liste.="</option>";
	 }

	return $liste;
	}
}
