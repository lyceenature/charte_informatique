<?php
include_once 'metier/PM.php';
/**
 * 
 *Classe definissant les constantes de l'application
 * @author pascal LamY
 *
 */
 class Constantes extends PM{


/*
 * constante de connexion a la base de donnée
 */
	 const HOST="localhost";
	 const USER="login";
	 const PASSWORD="pwd";
	 const BASE="charte";
	 const TYPE="mysql";
	 
	 //gestion des exceptions
	const EXCEPTION_DB_LOGIN_FAILED="Erreur d'authentification";
	const EXCEPTION_SELECTPERSONNE="Une erreur s\'est produite lors de la selection d\'une personne en BDD";
	const EXCEPTION_SELECTVIDEPERSONNE="Pazs de personnes en BDD";

	const EXCEPTION_INSERTMESSAGE="Une erreur s\'est produite lors de l'insertion  d\'un message en BDD";
	const EXCEPTION_UPDATE_PERSONNE="Une erreur s\'est produite lors de la mise a jour d\'une  personne en BDD";
	//connexion PDO
	const STR_CONNEXION = "Constantes::TYPE.':host='.Constantes::HOST.';dbname='.Constantes::BASE"; 
    const ARR_EXTRA_PARAMETER ="array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'";
    
    //courriel email
    const AUTH_USERMAIL="pascal.lamy";
    const AUTH_PASSMAIL="mot de passe";
    const HOST_MAIL="submission.educagri.fr";
    
    const MESSAGE1='<p>Bonjour,</p> <p>Vous  trouverez  ci-joint le lien vous permettant de consulter et d\'approuver la charte informatique du lyc&eacute;e:</p>';
    const MESSAGE2='<p>NOTA BENE: Tant que la charte ne sera pas valid&eacute;e, vous recevrez automatiquement un mail de rappel quotidiennement.</p>';
    const MESSAGE3= '<p>Vous pouvez consulter cette charte 	&agrave; tout moment sur l\'intranet sur le lien suivant: http://srvweb.lyceenature.com/charte/charte.pdf </p>';
    const MESSAGE4= '<p>Ce mail est automatique , Merci de ne pas y repondre.</p> Merci.<br/><br/> Cordialement, <br/> <br/>Karine Lambin';
    const SUJET='Charte Informatique';
	const FROM ='karine.lambin@educagri.fr';
	const FROMALIAS='Karine Lambin - enseignante TIM';
	const NOREPLY='noreply@educagri.fr';
	const NOREPLYALIAS='NOREPLY';
	const SITECHARTE="http://srvweb.lyceenature.com/charte/charte.php?email=";
	
}


