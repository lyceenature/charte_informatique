<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Gestion SIM Charte Info</title>
<link rel="stylesheet" type="text/css"
	href="js/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="js/themes/icon.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="js/easyloader.js"></script>
<script type="text/javascript">
$('document').ready(function() {
   easyloader.locale = 'fr';
});
</script>
</head>
<body>
	
<img src="images/logo.png" width="279" height="165" border="0"
	alt="Lycée Nature" title="" />
	<h2>Gestion de la Charte Informatique</h2>
	
<?php
include "utilAuth.php";
$e = isset($_GET['error']) ? strval($_GET['error']) : null;
if($e!=null){
	if($e=='loginv'){
		$mes="Veuillez saisir un login et un mot de passe!";
	}
	else if($e=='login'){
		$mes=" Erreur de saisie de login ou de mot de passe! Veuillez recommencer.";
	}
	else if($e=='logine'){
		$mes=" Votre session a expir&eacute;! Veuillez vous reconnecter.";
	}
}
else{
	$mes="Veuillez vous authentifier.";
}
utilAuth::mesInfo($mes);
?>

<div style="margin: 10px 0;"></div>
<form id="ff" method="POST" action="valid.php">
<div class="easyui-panel" title="Authentification" style="width: 400px">
<div style="padding: 10px 0 10px 60px">

<table>
	<tr>
		<td>Login:</td>
		<td><input class="easyui-validatebox" type="text" name="name"
			data-options="required:true"></td>
	</tr>
	<tr>
		<td>Mot de passe:</td>
		<td><input class="easyui-validatebox" type="password" name="pwd"
			data-options="required:true"></td>
	</tr>

</table>

</div>
<div style="text-align: center; padding: 5px"><input type="submit"
	class="easyui-linkbutton" value="Connexion" style="width:100px;border:1px solid #ccc;padding:2px;"> <input type="reset"
	class="easyui-linkbutton" value="Effacer" style="width:100px;border:1px solid #ccc;padding:2px;"></div>
	</div>
</form>



</div>

</body>
</html>
