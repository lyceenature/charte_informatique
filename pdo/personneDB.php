<?php
require_once "pmDB.php";
require_once "Constantes.php";
/**
 *
 *Classe permettant d'acceder en bdd pour authentifcation
 *
 * @author pascal
 *
 */
class personneDB extends pmDB
{
	private $db; // Instance de PDO

	public function __construct($db)
	{
		$this->db=$db;;
	}
	/**
	 *
	 * fonction de selection de l'objet personne en base de donnee
	 * @param $a
	 */
	public function selectPersonne($valide,$nom,$sort,$order,$offset,$rows)
	{
		$cond2='';
		$cond=" order by $sort $order limit $offset,$rows";
		if($nom !=''){
			$cond2="and p.nom like ".$nom;
		}
		$q = $this->db->prepare('select distinct  id , nom, prenom,email,date_charte, valide from users  where valide=:r'.$cond2.$cond );
		$q->bindValue(':r', $valide);

		$q->execute();
		$arrAll = $q->fetchAll();
		if(empty($arrAll)){
			throw new Exception(Constantes::EXCEPTION_SELECTPERSONNE);
		}

		$result=$arrAll;

		$q->closeCursor();
		$q = NULL;
		//retour du resultat

		return $result;
	}	
	public function selectPersonneMessage()
	{
	
		$q = $this->db->prepare('select  distinct id, email from users  where valide=0');
	

		$q->execute();
		$arrAll = $q->fetchAll(PDO::FETCH_ASSOC);
		if(empty($arrAll)){
			throw new Exception(Constantes::EXCEPTION_SELECTVIDEPERSONNE);
		}

		$result=$arrAll;

		$q->closeCursor();
		$q = NULL;
		//retour du resultat

		return $result;
	}	
	
	public function selectTokenPersMessage()
	{
	
		$q = $this->db->prepare('select  distinct u.email as email, s.token as token from users u, session s  where u.valide=0 and s.id_pers=u.id');
	

		$q->execute();
		$arrAll = $q->fetchAll(PDO::FETCH_ASSOC);
		if(empty($arrAll)){
			throw new Exception(Constantes::EXCEPTION_SELECTVIDEPERSONNE);
		}

		$result=$arrAll;

		$q->closeCursor();
		$q = NULL;
		//retour du resultat

		return $result;
	}	

	public function verifTokenPersMessage($t)
	{
	
		$q = $this->db->prepare('select  token from  session   where token=:t');
		$q->bindValue(':t', $t);

		$q->execute();
		$arrAll = $q->fetchAll(PDO::FETCH_ASSOC);
		if(empty($arrAll)){
			return false;
		}

		

		return true;
	}	
	
	public function insertTokenPers($i,$t){
		$q = $this->db->prepare('INSERT INTO session(id_pers,token,date_token) values(:i,:t,now())');
		$q->bindValue(':i', $i);
		$q->bindValue(':t', $t);

		$q->execute();
		$q->closeCursor();
		$q = NULL;
	}
	
	public function insertPersonne($n,$p,$e,$v){
		$q = $this->db->prepare('INSERT INTO users(nom,prenom,email,date_charte,valide) values(:n,:p,:e,now(),:v)');
		$q->bindValue(':n', $n);
		$q->bindValue(':p', $p);
		$q->bindValue(':e', $e);
		$q->bindValue(':v', $v);
		$q->execute();
		$q->closeCursor();
		$q = NULL;
	}
	public function deletePersonne($id){
		$q = $this->db->prepare('delete from users where id=:i');
		$q->bindValue(':i', $id);
		$q->execute();
		$q->closeCursor();
		$q = NULL;
	}
public function selectPersonneId($id)
	{
	
		$q = $this->db->prepare('select distinct   nom, prenom,email,date_charte,valide
		 from users where id=:i');
		$q->bindValue(':i', $id);

		$q->execute();
		$arrAll = $q->fetchAll();
		if(empty($arrAll)){
			throw new Exception(Constantes::EXCEPTION_SELECTPERSONNE);
		}

		$result=$arrAll;

		$q->closeCursor();
		$q = NULL;
		//retour du resultat

		return $result;
	}
	
	public function updatePersonne($id,$n,$p,$e,$v){
		try{
			$q = $this->db->prepare('update  users set  nom=:n, prenom=:p,email=:e,valide=:v,date_charte=now() where id=:i');
			
			$q->bindValue(':i', $id);
			
			$q->bindValue(':n', $n);
			$q->bindValue(':p', $p);
			$q->bindValue(':e', $e);
			$q->bindValue(':v', $v);
			

			$q->execute();
			$q->closeCursor();

			$q = NULL;
		}
		catch (Exception $e){

			throw new Exception(Constantes::EXCEPTION_UPDATE_PERSONNE . $e->getMessage());
		}
	}
		public function updatePersonneEmail($e){
		try{
			$q = $this->db->prepare('update  users set  valide=1,date_charte=now() where email=:e');
			
			
			$q->bindValue(':e', $e);
			
			

			$q->execute();
			$q->closeCursor();

			$q = NULL;
		}
		catch (Exception $e){

			throw new Exception(Constantes::EXCEPTION_UPDATE_PERSONNE . $e->getMessage());
		}
	}
	public function selectListePersonne($role,$sort,$order,$offset,$rows)
	{
		
		$cond=" order by $sort $order limit $offset,$rows";
	
		$q = $this->db->prepare('select   id , nom, prenom from personne  where id_role=:r'.$cond );
		$q->bindValue(':r', $role);

		$q->execute();
		$arrAll = $q->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_ASSOC);
		if(empty($arrAll)){
			throw new Exception(Constantes::EXCEPTION_SELECTPERSONNE);
		}

		

		$q->closeCursor();
		$q = NULL;
		//retour du resultat

		return $arrAll;
	}


}
