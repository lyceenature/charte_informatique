<?php
require_once "pmDB.php";
require_once "Constantes.php";
/**
 *
 *Classe permettant d'acceder en bdd pour authentifcation
 *
 * @author pascal
 *
 */
class loginDB extends pmDB
{
	private $db; // Instance de PDO

	public function __construct($db)
	{
		$this->db=$db;;
	}
	/**
	 *
	 * fonction de selection de l'objet login en base de donnee
	 * @param $a
	 */
	public function select($login,$pwd)
	{
		$q = $this->db->prepare('select id,nom, prenom,id_role from personne where login=:l and pwd=:p ');
		$q->bindValue(':l', $login);
		$q->bindValue(':p', $pwd);
		$q->execute();
		$arrAll = $q->fetch(PDO::FETCH_ASSOC);
		if(empty($arrAll)){
			throw new Exception(Constantes::EXCEPTION_DB_LOGIN_FAILED);
		}
		$result=$arrAll;

		$q->closeCursor();
		$q = NULL;
		//retour du resultat

		return $result;
	}
	public function selectP($login)
	{
		$q = $this->db->prepare('select id,nom, prenom,id_role,pwd from personne where login=:l');
		$q->bindValue(':l', $login);

		$q->execute();
		$arrAll = $q->fetch(PDO::FETCH_ASSOC);
		if(empty($arrAll)){
			throw new Exception(Constantes::EXCEPTION_DB_LOGIN_FAILED);
		}
		$result=$arrAll;

		$q->closeCursor();
		$q = NULL;
		//retour du resultat

		return $result;
	}
}
