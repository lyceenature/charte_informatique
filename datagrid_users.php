<?php
include_once "util.php";
if(!util::auth()){
	header('Location: index.php?error=logine');
}
?>

<script language="javascript" type="text/javascript">
$(function(){
	$('#dg').datagrid({
		onDblClickCell:function(field,row){
		consultUser();
		  }
		});
	$('#fm').form({
		url:'validUserAdd.php',
		onSubmit:function(){
		   
			return $(this).form('validate');
	
				
		},
		success:function(data){
			closeWindow();
			$.messager.alert('Information', data, 'info');
			$('#dg').datagrid('reload'); 
			
		
			
		}
	});
	$('#fmo').form({
		url:'validUserEdit.php',
		onSubmit:function(){
		
			return $(this).form('validate');			
		},
		success:function(data){
			closeWindow();
			$.messager.alert('Information', data, 'info');
			$('#dg').datagrid('reload'); 
			
		
			
		}
	});
		$('#fmd').form({
		url:'validUserDel.php?',
		onSubmit:function(){				
		},
		success:function(data){
			closeWindow();
			$.messager.alert('Information', data, 'info');
			$('#dg').datagrid('reload'); 
	
		}
	});
});

function getDetail(index){
alert('ID:'+index);	
}
function closeWindow(){

					$('#dlg').dialog('close');		// close the dialog		
					$('#dlr').dialog('close');	
					$('#dld').dialog('close');	
					$('#dll').dialog('close');	
					$('#dlc').dialog('close');	
				
			}
function doSearch(){
	$('#dg').datagrid('load',{
		valide: $('#valide').val(),
		nom: $('#nom').val()
	});
}
	function getSelected(){
	if (row){
		return row.id;
	}
}

	function consultUser(){

		var row = $('#dg').datagrid('getSelected');
		if (row){
			$('#dlc').dialog('open').dialog('setTitle','Consultation de l\' utilisateur '+row.prenom + ' ' +row.nom);
			$('#fmc').form('load',row);	
			$('#ld').datagrid('load',{
				id : row.id
			});	
		}
	}		
	function editUser(){

		var row = $('#dg').datagrid('getSelected');
		if (row){
			$('#dlr').dialog('open').dialog('setTitle','Modification de l\' utilisateur '+row.prenom + ' ' +row.nom);
			$('#fmo').form('load',row);		
		}
	}	
	function ajoutUser(){
		
		  $('#dlg').form('clear');   
			$('#dlg').dialog('open').dialog('setTitle','Ajouter un utilisateur');
				
 
	}
	function delUser(){
		
		var row = $('#dg').datagrid('getSelected');
		if (row){
		$('#dld').dialog('open').dialog('setTitle','Suppression de l\' utilisateur '+row.prenom + ' ' +row.nom);
		$('#fmd').form('load',row);		
		}	
	
}				
		function formatDetail(val,row) {
	

		$res2 = "<a title='Editer' href=\"#\" onclick=\"editUser()\"><img src=\"images/pencil.png\" border=\"0\"></a> ";
		$res3 = "<a title='Supprimer' href=\"#\" onclick=\"delUser()\"><img src=\"images/cancel.png\" border=\"0\"></a>";	

	return $res2 + $res3;
			}

	</script>



<div title="Liste des utilisateurs" style="padding: 10px;">

<table id="dg" class="easyui-datagrid"
	style="width: 100 %; height: 600px" url="datagrid_getusers.php?id=<?php echo $_GET['id'] ?>"
	title="Liste des Utilisateurs"
	loadMsg="Chargement en Cours,Veuillez patienter..." singleSelect="true"
	rownumbers="true" pagination="false" toolbar="#tb" fitColumns="true" striped="true">
	<thead>
		<tr>
			
			<th field="nom" width="100" sortable="true">Nom</th>
			<th field="prenom" width="100" sortable="false">Prenom</th>
		
			<th field="email" width="100"  sortable="true">Email</th>
			<th field="date_charte" width="100"  sortable="true">Date</th>
			
			<th field="detail" width="80" align="center" sortable="false"
				formatter="formatDetail">D&eacute;tail</th>
		</tr>
	</thead>
</table>
</div>

<div id="tb"><select id="valide"
	style="width: 100px; line-height: 26px; border: 1px solid #ccc"
	onchange="doSearch()">
	<option value='1'>Charte validée</option>
	<option value='0'>Charte non validée</option>
</select> <a href="#" class="easyui-linkbutton" plain="true"
	 onclick="ajoutUser()"><img src="images/ajoutP.png" border=\"0\">Ajouter un Utilisateur</a></div>
<!--formulaire ajout utilisateur-->
<div id="dlg" class="easyui-dialog"
	style="width: 500px; height: 450px; padding: 10px 20px" closed="true"
	buttons="#dlg-buttons2">

<form id="fm" method="post">
<table>

	<tr>
		<td class='dv-label'>Nom:</td>
		<td><input name="nom" type="text" class="easyui-validatebox"
			data-options="required:true"></td>
	</tr>
	<tr>
		<td class='dv-label'>Pr&eacute;nom:</td>
		<td><input name="prenom" type="text" class="easyui-validatebox"
			data-options="required:true"></td>
	</tr>
	<tr>
		<td class='dv-label'>Email:</td>
		<td><input name="email" type="text" class="easyui-validatebox"
			data-options="required:true,validType:'email'"></td>
	</tr>
	<tr>
		<td class='dv-label'>Charte:</td>
		<td><select name="valide" class="easyui-combobox"
			data-options="required:true">
			<option value='1'>Validée</option>
			<option value='0'>Non Validée</option>
		</select></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
<table>
	<tr>
		<td><input type="submit" value="Ajouter"></td>
		<td><input type="button" value="Annuler" onclick="closeWindow()"></td>
	</tr>
</table>
</form>
</div>
<!--formulaire Consult utilisateur-->
<div id="dlc" class="easyui-dialog"
	style="width: 550px; height: 550px; padding: 10px 20px" closed="true">
 <div class="easyui-accordion" id="infoclient"  style="padding:5px;width:480px;height:470px;">
<div  title="Informations utilisateur"  selected ="true" style="overflow:auto;padding:10px;">

<form id="fmc" method="post">
<table>
	<tr>
		<td class='dv-label'>R&eacute;f&eacute;rence:</td>
		<td><input name="id" type="text" class="easyui-validatebox">
			 </td>
	</tr>
	
	<tr>
		<td class='dv-label'>Nom:</td>
		<td><input name="nom" type="text" class="easyui-validatebox"></td>
	</tr>
	<tr>
		<td class='dv-label'>Pr&eacute;nom:</td>
		<td><input name="prenom" type="text" class="easyui-validatebox"></td>
	</tr>
	
	<tr>
		<td class='dv-label'>Email:</td>
		<td><input name="email" type="text" class="easyui-validatebox"></td>
	</tr>
	<tr>
		<td class='dv-label'>Charte:</td>
		<td><select name="valide" class="easyui-validatebox">
			<option value='1'>Validée</option>
			<option value='0'>Non Validée</option>
		</select></td>
	</tr>
	
	
</table>
</div>
<input type="hidden" name="id">


 </div>

<table>

	<tr>

	<td><input type="button" value="Fermer" onclick="closeWindow()"></td>
	</tr>
</table>
</form>

</div>
<!--formulaire Modif utilisateur-->
<div id="dlr" class="easyui-dialog"
	style="width: 500px; height: 490px; padding: 10px 20px" closed="true">

<form id="fmo" method="post">
<table>
	<tr>
		<td class='dv-label'>R&eacute;f&eacute;rence:</td>
		<td><input name="id" type="text" disabled="disabled"
			class="easyui-validatebox" data-options="required:false"></td>
	</tr>
	
	<tr>
		<td class='dv-label'>Nom:</td>
		<td><input name="nom" type="text" class="easyui-validatebox"
			data-options="required:true"></td>
	</tr>
	<tr>
		<td class='dv-label'>Pr&eacute;nom:</td>
		<td><input name="prenom" type="text" class="easyui-validatebox"
			data-options="required:true"></td>
	</tr>
	<tr>
		<td class='dv-label'>Email:</td>
		<td><input name="email" type="text" class="easyui-validatebox"
			data-options="required:true,validType:'email'"></td>
	</tr>
	<tr>
		<td class='dv-label'>Charte:</td>
		<td><select name="valide" class="easyui-combobox"
			data-options="required:true">
			<option value='1'>Validée</option>
			<option value='0'>NonValidée</option>
		</select></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
<input type="hidden" name="id">

<table>
	<tr>
		<td><input type="submit" value="Modifier"></td>
		<td><input type="button" value="Annuler" onclick="closeWindow()"></td>
	</tr>
</table>


</form>
</div>
<!--formulaire suppression utilisateur-->
<div id="dld" class="easyui-dialog"
	style="width: 500px; height: 200px; padding: 10px 20px" closed="true">

<form id="fmd" method="post">
<table>
	<tr>
		<td colspan="2" class='dv-label'>Etes vous sur de vouloir supprimer
		cet utilisateur?</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>

<table>
	<input type="hidden" name="id"/>
	<tr>
	
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><input type="submit" value="Supprimer"></td>
		<td><input type="button" value="Annuler" onclick="closeWindow()"></td>
	</tr>
</table>

</form>
</div>


</body>
</html>
